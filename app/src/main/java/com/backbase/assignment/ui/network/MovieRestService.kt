package com.backbase.assignment.ui.network

import com.backbase.assignment.ui.model.Base
import com.backbase.assignment.ui.model.DetailsBase
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import io.reactivex.Single
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import java.io.IOException
import java.util.concurrent.TimeUnit

interface MovieRestService {
    companion object {
        val instance: MovieRestService by lazy {
            //Logging
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            //OkHttp
            val okHttpClient = OkHttpClient.Builder()
                .readTimeout(10000, TimeUnit.MILLISECONDS)
                .writeTimeout(1000, TimeUnit.MILLISECONDS)
                .addInterceptor(loggingInterceptor)
                .addInterceptor(object : Interceptor {
                    @Throws(IOException::class)
                    override fun intercept(chain: Interceptor.Chain): Response {
                        val original = chain.request()
                        val requestBuilder = original.newBuilder()
                        .addHeader("Accept", "application/json")
                        .addHeader("Request-Type", "Android")
                        .addHeader("Content-Type", "application/json")
                        val request = requestBuilder.build()
                        return chain.proceed(request)
                    }
                }).build()
            //Retrofit
            val retrofit = Retrofit.Builder()
                .baseUrl("https://api.themoviedb.org/")
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            retrofit.create(MovieRestService::class.java)
        }
    }

    @GET("3/movie/now_playing")
    fun getMovies(
        @Query("language") language: String = "en-US",
        @Query("page") page: String = "undefined",
        @Query("api_key") api_key: String = "55957fcf3ba81b137f8fc01ac5a31fb5"
    ): Single<Base>

    @GET("3/movie/{movieID}")
    fun getMovieDetails(
        @Path("movieID") movieID:Int,
        @Query("api_key") api_key: String = "b0720091062636652b7d37366381b4df"
    ): Single<DetailsBase>
}