package com.backbase.assignment.ui.movie

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.backbase.assignment.R
import com.backbase.assignment.ui.model.Results
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.horizontal_movie_item.view.*
import kotlinx.android.synthetic.main.movie_item.view.*

class HorizontalMoviesAdapter(private var movieList: ArrayList<Results>) :
    RecyclerView.Adapter<HorizontalMoviesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.horizontal_movie_item,
                parent,
                false
            )
        )
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(movieList[position])
    override fun getItemCount() = movieList.size
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: Results) = with(itemView) {
            var st = item.poster_path
            Picasso.get().load(Uri.parse("https://image.tmdb.org/t/p/original/${st}")).placeholder(R.drawable.avatar_12)
                .into(itemView.imageViewH)

        }
    }
}