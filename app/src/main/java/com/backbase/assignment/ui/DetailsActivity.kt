package com.backbase.assignment.ui

import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.backbase.assignment.R
import com.backbase.assignment.ui.model.DetailsBase
import com.backbase.assignment.ui.model.Genres
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.movie_details.*
import kotlinx.android.synthetic.main.movie_item.view.*
import java.lang.StringBuilder

class DetailsActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.movie_details)

        val details: DetailsBase? = intent.getParcelableExtra("par")

        Log.d("tag87", details.toString())
           textView.text = details?.title.toString()

        var st = details?.poster_path
        Picasso.get().load("https://image.tmdb.org/t/p/w500${st}").placeholder(R.drawable.avatar_12).into(imageView)

        overview.text = details?.overview
        genres.text = buildGenresString(details?.genres)
    }

    private fun buildGenresString(genres:List<Genres>?):String {
        val builder = StringBuilder()
        if(genres!=null) {
            for (genre in genres) {
                builder.append(genre.toString())
                builder.append(" ")
            }
        }
        return builder.toString()
    }

}