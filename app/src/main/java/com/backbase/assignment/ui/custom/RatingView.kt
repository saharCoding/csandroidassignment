package com.backbase.assignment.ui.custom

import android.content.Context
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.View


class RatingView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {
    var progress = 360f
    var arcStar = 270f
    var arcEnd = 360f
    var rotateStep = 0.2
    var bitmap: Bitmap? = null
    var totalTime = 0
    var image: Bitmap? = null
    var drawable: Drawable? = null
    var boundWidth = 5
    var progressWidth = 30
    private var isRotate = false
    var progressColor = Color.GREEN
    var progressBackColor = Color.GREEN
    private val rotateDegree = 0f
    private var radius = 0f
    private val paint = Paint(Paint.ANTI_ALIAS_FLAG)

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        val paddingLeft = paddingLeft
        val paddingRight = paddingRight
        val paddingTop = paddingTop
        val paddingBottom = paddingBottom

        //get the view's width and height and decide the radiu

        //get the view's width and height and decide the radiu
        val width = width - paddingLeft - paddingRight
        val height = height - paddingTop - paddingBottom
        radius = (Math.min(width, height) / 2 - boundWidth - progressWidth).toFloat()

        //setup the paint

        //setup the paint
        paint.style = Paint.Style.STROKE
        paint.strokeWidth = boundWidth.toFloat()
        paint.color = Color.BLACK

        //draw the inner circle

        //draw the inner circle
        val centerX = paddingLeft + getWidth() / 2
        val centerY = paddingTop + getHeight() / 2
        canvas!!.drawCircle(centerX.toFloat(), centerY.toFloat(), radius, paint)


        val totalRadiu: Float = radius + boundWidth + progressWidth / 2

        paint.strokeWidth = progressWidth.toFloat()
        paint.strokeCap = Paint.Cap.ROUND

        //prepare for draw arc

        //prepare for draw arc
        val oval = RectF()
        oval.left = centerX - totalRadiu
        oval.top = centerY - totalRadiu
        oval.right = centerX + totalRadiu
        oval.bottom = centerY + totalRadiu
        paint.color = progressBackColor

        //draw background arc

        //draw background arc
        canvas!!.drawArc(oval, arcStar, arcEnd, false, paint)

        //draw progress arc

        //draw progress arc
        paint.color = progressColor
        canvas!!.drawArc(oval, arcStar, progress, false, paint)

    }

}