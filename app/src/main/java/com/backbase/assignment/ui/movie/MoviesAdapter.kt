package com.backbase.assignment.ui.movie

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.backbase.assignment.R
import com.backbase.assignment.ui.custom.RatingView
import com.backbase.assignment.ui.model.Results
import com.jakewharton.rxbinding.widget.RxProgressBar.progress
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.movie_item.view.*


class MoviesAdapter(private var movieList: ArrayList<Results>, private val clickListener: (Results) -> Unit) :
    RecyclerView.Adapter<MoviesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.movie_item,
                parent,
                false
            )
        )
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(movieList[position],clickListener)

    override fun getItemCount() = movieList.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(
            item: Results,
            clickListener: (Results) -> Unit
        ) = with(itemView) {
            itemView.title.text = item.title

            itemView.releaseDate.text = item.release_date
            var st = item.poster_path
            Picasso.get().load(Uri.parse("https://image.tmdb.org/t/p/original/${st}"))
                .into(itemView.poster)

            itemView.releaseDate.text = item.release_date
            setUpRating(itemView.rating, (360 * (item.popularity / 100)).toFloat())
            itemView.setOnClickListener { clickListener(item)}
        }

        fun setUpRating(ratingView: RatingView, actualProgres: Float): Unit {
            ratingView.progressColor = itemView.context.resources.getColor(R.color.colorPrimary)
            ratingView.progressBackColor = itemView.context.resources.getColor(R.color.colorPrimaryDark)
            ratingView.boundWidth = 0
            ratingView.progressWidth = 30
            ratingView.progress = actualProgres
        }
    }
}
