package com.backbase.assignment.ui.model

import com.backbase.assignment.ui.network.MovieRestService
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MovieRepositoryImp(private val movieRestService: MovieRestService) :MovieRepository{

    override fun getMovieList(): Single<Base> {
       return movieRestService.getMovies().subscribeOn(Schedulers.io())
           .observeOn(AndroidSchedulers.mainThread())
    }

    override fun getMovieDetais(movieId:Int): Single<DetailsBase> {
        return movieRestService.getMovieDetails(movieId).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())    }

}