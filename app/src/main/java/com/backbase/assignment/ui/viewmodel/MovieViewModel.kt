package com.backbase.assignment.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.backbase.assignment.ui.model.Base
import com.backbase.assignment.ui.model.DetailsBase
import com.backbase.assignment.ui.model.MovieRepository
import io.reactivex.disposables.CompositeDisposable
import java.net.UnknownHostException

class MovieViewModel(private val movieRepository: MovieRepository) : ViewModel() {

    private val disposable = CompositeDisposable()
    private val stateMutableLiveData = MutableLiveData<AppState>()
    val stateLiveData: LiveData<AppState> get() = stateMutableLiveData
    var loaded = false

    fun getMovies() {
        stateMutableLiveData.value = AppState.LOADING
        disposable.add(
            movieRepository.getMovieList()
                .subscribe({
                    loaded = true
                    if (it == null) {
                        stateMutableLiveData.value = AppState.ERROR("No movies Retrieved!")
                    } else {

                        stateMutableLiveData.value = AppState.SUCCESS(it)
                    }
                }, {
                    loaded = true
                    val errorString = when (it) {
                        is UnknownHostException -> it.localizedMessage
                        else -> it.localizedMessage
                    }
                    stateMutableLiveData.value = AppState.ERROR(errorString)
                })
        )
    }

    fun getMovieDetails(movieId:Int) {
        stateMutableLiveData.value = AppState.LOADING
        disposable.add(
            movieRepository.getMovieDetais(movieId)
                .subscribe({
                    loaded = true
                    if (it == null) {
                        stateMutableLiveData.value = AppState.ERROR("No movies Retrieved!")
                    } else {

                        stateMutableLiveData.value = AppState.SUCCESSFUL(it)
                    }
                }, {
                    loaded = true
                    val errorString = when (it) {
                        is UnknownHostException -> it.localizedMessage
                        else -> it.localizedMessage
                    }
                    stateMutableLiveData.value = AppState.ERROR(errorString)
                })
        )
    }
    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
    sealed class AppState {
        object LOADING : AppState()
        data class SUCCESS(val movieList: Base) : AppState()
        data class SUCCESSFUL(val movieDetail: DetailsBase) : AppState()
        data class ERROR(val message: String) : AppState()
    }
}

