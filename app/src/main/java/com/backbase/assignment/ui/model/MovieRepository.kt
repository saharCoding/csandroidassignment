package com.backbase.assignment.ui.model

import io.reactivex.Single

interface MovieRepository {

    fun getMovieList(): Single<Base>
    fun getMovieDetais(movieId:Int): Single<DetailsBase>


}