package com.backbase.assignment.ui

import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.backbase.assignment.R
import com.backbase.assignment.ui.model.Base
import com.backbase.assignment.ui.model.DetailsBase
import com.backbase.assignment.ui.model.Results
import com.backbase.assignment.ui.movie.HorizontalMoviesAdapter
import com.backbase.assignment.ui.movie.MoviesAdapter
import com.backbase.assignment.ui.viewmodel.MovieViewModel
import com.backbase.assignment.ui.viewmodel.MovieViewModelFactory
import com.example.w1d3_rxjavademo.inject.Injection


class MainActivity : AppCompatActivity() {

    lateinit var viewModelFactory: MovieViewModelFactory
    lateinit var viewModel: MovieViewModel
    private val injection = Injection()

    var horizontalMoviesAdapter: HorizontalMoviesAdapter? = null
    var moviesAdapter: MoviesAdapter? = null

    private lateinit var hRecyclerView: RecyclerView

    private lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel = ViewModelProvider(
            this,
            MovieViewModelFactory(injection.provideUserRepo())
        ).get(MovieViewModel::class.java)



        viewModel.getMovies()

        viewModel.stateLiveData.observe(this, Observer { appState ->
            when (appState) {
                is MovieViewModel.AppState.LOADING -> displayToast("Loading")
                is MovieViewModel.AppState.SUCCESS -> {
                    displayMovies(appState.movieList)
                }
                is MovieViewModel.AppState.SUCCESSFUL -> {
                    displayDetails(appState.movieDetail)
                }
                is MovieViewModel.AppState.ERROR -> displayToast("error!!")
                else -> displayToast("Something Went Wrong... Try Again.")
            }
        })
    }

    private fun initHorizontalRecyclerView(results: ArrayList<Results>) {

        hRecyclerView = findViewById(R.id.recyclerView)
        hRecyclerView.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)

        horizontalMoviesAdapter = HorizontalMoviesAdapter(results)
        hRecyclerView.adapter = horizontalMoviesAdapter

    }

    private fun initVerticalRecyclerView(results: ArrayList<Results>) {

        recyclerView = findViewById(R.id.verticalRecyclerView)
        recyclerView.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        moviesAdapter = MoviesAdapter(results) { hourly: Results ->
            onMovieSelected(hourly)
        }
        recyclerView.adapter = moviesAdapter

    }

    private fun onMovieSelected(movie: Results) {
        viewModel.getMovieDetails(movie.id)
    }

    private fun displayMovies(movieList: Base) {

        initHorizontalRecyclerView(movieList.results)
        initVerticalRecyclerView(movieList.results)
    }

    private fun displayDetails(movieDetails: DetailsBase) {

        val intent = Intent(this@MainActivity, DetailsActivity::class.java)
        intent.putExtra("par",movieDetails)
        startActivity(intent)

    }

    private fun displayToast(message: String?) {
        Toast.makeText(
            applicationContext, message,
            Toast.LENGTH_SHORT
        ).show()
    }
}
