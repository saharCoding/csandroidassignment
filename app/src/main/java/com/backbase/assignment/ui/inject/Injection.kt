package com.example.w1d3_rxjavademo.inject

import com.backbase.assignment.ui.model.MovieRepository
import com.backbase.assignment.ui.model.MovieRepositoryImp
import com.backbase.assignment.ui.network.MovieRestService

class Injection {

    private var userRestService: MovieRestService? = null

    fun provideUserRepo(): MovieRepository {
        return MovieRepositoryImp(provideTicketRestService())
    }

    private fun provideTicketRestService(): MovieRestService {
        if (userRestService == null) {
            userRestService = MovieRestService.instance
        }
        return userRestService as MovieRestService
    }
}